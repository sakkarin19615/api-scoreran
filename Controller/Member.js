const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const jwt = require('jsonwebtoken');
const checkdata = require("../verifydata/verifydata");
var moment = require('moment');
const helper = require('../helper/helper');

const ModelMember = require("../model/modelmember.js");
const e = require('express');
var date = Date.now()

const create_member = async (req, res) => {
    const obj = {
        name: req.body.name,
        playrate: 0,
        win_rate: 0,
        is_delete: 1,
        status_publish: 1,
        ref_member_scoreran_id: req.user_id
    }

    datareturn = await ModelMember.Modelcreate_member(obj)

    if (datareturn.status === true) {
        res.status(200).json({
            status: datareturn.status,
            message: datareturn.message,
            data: datareturn.result,
            result: true

        });
    } else {
        res.status(400).json({
            status: datareturn.status,
            message: datareturn.message,
            data: null,
            result: false
        });
    }

};

const list_rate = async (req, res) => {
    const obj = {
        id: req.user_id
    }
    datareturn = await ModelMember.Modellist_rate(obj)
    if (datareturn.status === true) {
        res.status(200).json({
            status: datareturn.status,
            message: datareturn.message,
            data: datareturn.result,
            result: true
        });
    } else {
        res.status(400).json({
            status: datareturn.status,
            message: datareturn.message,
            result: false
        });
    }
};

const list_member = async (req, res) => {
    const obj = {
        id: req.user_id
    }
    // ckday = await ModelMember.ModelSelectAdmin(obj)
    // ddnow = moment(Date.now()).format('YYYY-MM-DD');
    // if (ddnow != moment(ckday.result.playday).format('YYYY-MM-DD')) {
    //     clean = await ModelMember.Modelclean(obj)
    //     upday = await ModelMember.Modelupday(obj)
    // } else {

    // }
    
    datareturn = await ModelMember.Modellist_member(obj)
    if (datareturn.status === true) {
        res.status(200).json({
            status: datareturn.status,
            message: datareturn.message,
            data: datareturn.result,
            result: true
        });
    } else {
        res.status(400).json({
            status: datareturn.status,
            message: datareturn.message,
            result: false
        });
    }
};

const random = async (req, res) => {
    const obj2 = {
        id: req.user_id
    }

    // ckday = await ModelMember.ModelSelectAdmin(obj2)
    // ddnow = moment(Date.now()).format('YYYY-MM-DD');
    // if (ddnow != moment(ckday.result.playday).format('YYYY-MM-DD')) {
    //     clean = await ModelMember.Modelclean(obj2)
    //     upday = await ModelMember.Modelupday(obj2)
    // } else {

    // }

    count = await ModelMember.Modelcount(obj2)
    var cc = count.result.countmember / 4
    var d = Math.ceil(cc)

    clean = await ModelMember.Modelclean(obj2)
    cleanroom = await ModelMember.Modelcleanroom(obj2)

    for (var i = 0; i < d; i++) {
        crateR = await ModelMember.Modelcrateroom(obj2)
        datarandom = await ModelMember.Modelrandom(obj2)
        sort = 0
        for (var ii = 0; ii < datarandom.result.length; ii++) {
            cc = datarandom.result[ii].playday_rate + 1
            sort = sort + 1
            const obj = {
                groups_id: crateR.result.insertId,
                scoreran_id: datarandom.result[ii].id,
                playday_rate: cc,
                sort: sort
            }
            
            update_playdayrate = await ModelMember.Modelupdate_playdayrate(obj)
            saveroom = await ModelMember.Modelsaveroom(obj)

            // รวม join
            // saveroom__update_playdayrate = await ModelMember.Modeljoin_saveroom__update_playdayrate(obj)
        };
    }

    res.status(200).json({
        status: true,
        message: "สุ่มสำเร็จ",
        result: true
    });;

};

const list_random = async (req, res) => {
    const obj = {
        id: req.user_id
    }
    datareturn = await ModelMember.Modellist_random(obj)
    var ct = []
    for (var i = 0; i < datareturn.result.length; i++) {
        const obj2 = {
            groups_id: datareturn.result[i].id
        }
        data_publish_shops = await ModelMember.Modelgetlistgroup(obj2)

        var packed = {
            groups_id: datareturn.result[i].id,
            status_play: datareturn.result[i].status_play,
            player: data_publish_shops.result
        }
        ct.push(packed)

    }
    if (datareturn.status === true) {
        res.status(200).json({
            status: datareturn.status,
            message: datareturn.message,
            data: ct,
            // data: datareturn.result,
            result: true
        });
    } else {
        res.status(400).json({
            status: datareturn.status,
            message: datareturn.message,
            result: false
        });
    }
};

const editstatus_publish = async (req, res) => {
    const obj = {
        id: req.params.scoreran_id
    };

    ck = await ModelMember.Modelgetscoreran_ck(obj)
    if (ck.result.status_publish == 1) {
        publish = 0
    }else{
        publish = 1
    }
    
    const obj2 = {
        id: req.params.scoreran_id,
        status_publish: publish
    };
    
    datareturn = await ModelMember.Modeleditpublish(obj2)


    if (datareturn.status === true) {
        res.status(200).json({
            status: datareturn.status,
            message: datareturn.message,
            // data: datareturn.result,
            result: true
        });
    } else {
        res.status(400).json({
            status: datareturn.status,
            message: datareturn.message,
            // data: null,
            result: false
        });
    }

};

const deletescoreran = async (req, res) => {
    const obj = {
        id: req.params.scoreran_id,
        member_id: req.user_id
    };

    datareturn = await ModelMember.Modeldeletescoreran(obj)
    dd = await ModelMember.Modelresetgroups(obj)


    if (datareturn.status === true) {
        res.status(200).json({
            status: datareturn.status,
            message: datareturn.message,
            // data: datareturn.result,
            result: true
        });
    } else {
        res.status(400).json({
            status: datareturn.status,
            message: datareturn.message,
            // data: null,
            result: false
        });
    }

};

const changeroom = async (req, res) => {
    const obj = {
        id: req.params.groups_id,
    };
    datareturn = await ModelMember.Modelchangestatusgroups(obj)
  
    if (datareturn.status === true) {
      res.status(200).json({
        status: datareturn.status,
        message: datareturn.message,
        result: true
      });
    } else {
      res.status(400).json({
        status: datareturn.status,
        message: datareturn.message,
        result: false
      });
    }
  
};

const addscoreresult = async (req, res) => {

    const obj = {
        id: req.body.groups_id,

    }
    ckscoreran = await ModelMember.Modelchangestatusgroups2(obj)
    for (var i = 0; i < req.body.player.length; i++) {
        const obj2 = {
            id: req.body.player[i].id
    
        }
        ckscoreran = await ModelMember.Modelgetscoreran_ck(obj2)

        win = ckscoreran.result.win_rate + req.body.player[i].win
        playrate = ckscoreran.result.playrate + 1

        const obj3 = {
            id: req.body.player[i].id,
            win_rate: win,
            playrate: playrate
    
        }
        datareturn = await ModelMember.Modeladdscoreresult(obj3)

    }

    if (datareturn.status === true) {
        res.status(200).json({
            status: datareturn.status,
            message: datareturn.message,
            result: true

        });
    } else {
        res.status(400).json({
            status: datareturn.status,
            message: datareturn.message,
            result: false
        });
    }

};

const scorerandetail = async (req, res) => {
    const obj = {
        id: req.user_id
    }
    datareturn = await ModelMember.ModelSelectAdmin(obj)
    if (datareturn.status === true) {
        res.status(200).json({
            status: datareturn.status,
            message: datareturn.message,
            data: datareturn.result,
            result: true
        });
    } else {
        res.status(400).json({
            status: datareturn.status,
            message: datareturn.message,
            data: null,
            result: false
        });
    }
};

module.exports = {
    create_member,
    list_member,
    list_rate,
    random,
    list_random,
    editstatus_publish,
    deletescoreran,
    changeroom,
    addscoreresult,
    scorerandetail
}
