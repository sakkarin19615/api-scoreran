const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const jwt = require('jsonwebtoken');
const checkdata = require("../verifydata/verifydata");
var moment = require('moment');
const helper = require('../helper/helper');
const modelsSuperadmin = require("../model/modelsSuperadmin");
const ModelMember = require("../model/modelmember.js");
var date = Date.now()
var fs = require('fs');

const SECRET = "member_scoreran";

const createAdmin = async (req, res) => {

    const obj = {
        username: req.body.username,
        password: req.body.password,
    }
    obj.password = await helper.encryptionData(obj.password)
    datareturn = await modelsSuperadmin.ModelcreateAdmin(obj)

    if (datareturn.status === true) {
        res.status(200).json({
            status: datareturn.status,
            message: datareturn.message,
            data: datareturn.result,
            result: true

        });
    } else {
        res.status(400).json({
            status: datareturn.status,
            message: datareturn.message,
            data: null,
            result: false
        });
    }



};

const login = async (req, res) => {

    const obj = {
        username: req.body.username,
        password: req.body.password
    }
    // obj.password = await helper.encryptionData(obj.password)
    datareturn = await modelsSuperadmin.ModelSelectAdmin(obj)
    if (datareturn.status == true) {

        check = await helper.decryptionData(req.body.password, datareturn.result.password)
        if (check == true) {
            try {
                const payload = {
                    member_id: datareturn.result.id,
                    status : 1
                };
                newtoken = jwt.sign({ payload }, SECRET, { noTimestamp: true })
                const obj = {
                    member_id: datareturn.result.id,
                }

                dataretoken = await modelsSuperadmin.ModelRemoveTokenAndCreate(obj, newtoken)

                // const obj2 = {
                //     id: datareturn.result.id,
                // }
                // ddnow = moment(Date.now()).format('YYYY-MM-DD');
                // if (ddnow != moment(datareturn.result.playday).format('YYYY-MM-DD')) {
                //     clean = await ModelMember.Modelclean(obj2)
                //     upday = await ModelMember.Modelupday(obj2)
                // } else {

                // }

                if (dataretoken.status == true) {
                    dataretoken.result["token"] = newtoken
                    res.status(200).json({
                        status: dataretoken.status,
                        message: dataretoken.message,
                        result: dataretoken.result
                    });
                } else {
                    res.status(400).json({
                        status: dataretoken.status,
                        message: dataretoken.message,
                        result: null
                    });
                }
            } catch (error) {
                res.status(400).json({
                    status: false,
                    message: error,
                });
            }
        } else {
            res.status(400).json({
                status: false,
                message: "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง",
            });
        }



    } else {
        res.status(400).json({
            status: false,
            message: "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง",
        });
    }

}

module.exports = {
    createAdmin,
    login
}
