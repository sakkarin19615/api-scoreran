const { Db } = require('mongodb');
const { DB } = require('../configs/db.connect');
var uuid4 = require('uuid4')
var moment = require('moment');


const ModelcreateAdmin = async function (obj) {
    ddnow = moment(Date.now()).format('YYYY-MM-DD');
    sql = "INSERT INTO member_scoreran (username,password,is_delete,playday) "+
    "values ('" + obj.username + "','" + obj.password + "',1,'"+ddnow+"')";

    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            if (ex) {
                // console.log(ex)
                if (ex.errno == 1062) {
                    message = 'ชื่อผู้ใช้งาน ถูกใช้งานแล้ว'
                } else {
                    message = ex.toString()
                }
                data = {
                    status: false,
                    message: message,
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: '',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        return err
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const ModelSelectAdmin = async function (obj) {
    sql = "select * from member_scoreran " +
        "where username = '" + obj.username + "' AND is_delete = 1 ";

    const rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            if (ex) {
                data = {
                    status: false,
                    message: ex.toString(),
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: '',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });
    var sta
    var re
    if (rows['result'].length != 0) {
        sta = rows['status']
        // re = rows['result'][0]
        re = Object.values(JSON.parse(JSON.stringify(rows['result'])))
        re = re[0]
    } else {
        sta = false
        re = []
    }
    return data = {
        status: sta,
        message: rows['message'],
        result: re
    }

};

const ModelRemoveTokenAndCreate = async function (obj, newtoken) {
    sql = "delete from member_scoreran_token where ref_member_scoreran_id = " + obj.member_id + "";
    const delete_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            if (ex) {
                data = {
                    status: false,
                    message: ex.toString(),
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: '',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        console.log(err)
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });
    // console.log(moment().format("YYYY-MM-DD HH:mm:ss"))
    sql = "INSERT INTO member_scoreran_token (access_token,ref_member_scoreran_id,create_date) values ('" + newtoken + "','" + obj.member_id + "','" + moment().format("YYYY-MM-DD HH:mm:ss") + "')";
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            if (ex) {
                data = {
                    status: false,
                    message: ex.toString(),
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: '',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        return err
    });

    return data = {
        status: insert_rows['status'],
        message: 'Login Success.',
        result: {
            member_id: obj.member_id
        }
    }
}

module.exports = {
    ModelcreateAdmin,
    ModelRemoveTokenAndCreate,
    ModelSelectAdmin

}