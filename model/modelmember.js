const { Db } = require('mongodb');
const { DB } = require('../configs/db.connect');
var uuid4 = require('uuid4')
var moment = require('moment');

const Modelcreate_member = async function (obj) {
    sql = "INSERT INTO scoreran (name,is_delete,playrate,win_rate,status_publish,ref_member_scoreran_id) " +
        "values ('" + obj.name + "'," + obj.is_delete + "," + obj.playrate + "," + obj.win_rate + "," + obj.status_publish + "," + obj.ref_member_scoreran_id + ")";
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            if (ex) {
                // console.log(ex)
                if (ex.errno == 1062) {
                    message = 'ชื่อผู้ใช้งาน ถูกใช้งานแล้ว'
                } else {
                    message = ex.toString()
                }
                data = {
                    status: false,
                    message: message,
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: 'Success',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        // console.log(err)
        return err
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modellist_member = async function (obj) {
    sql = "select * from scoreran where is_delete = 1 AND ref_member_scoreran_id = " + obj.id + " ORDER BY scoreran.name ASC";
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'Success',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    var ct = []
    var sortorder = 0
    for (var i = 0; i < insert_rows['result'].length; i++) {
        var sortorder = sortorder + 1
        if (insert_rows['result'][i].status_publish == 0) {
            status_publish = false
        } else {
            status_publish = true
        }
        if (insert_rows['result'][i].is_delete == 0) {
            is_delete = false
        } else {
            is_delete = true
        }
        var packed = {
            sortorder: sortorder,
            scoreran_id: insert_rows['result'][i].id,
            name: insert_rows['result'][i].name,
            status_publish: status_publish,
            is_delete: is_delete

        }
        ct.push(packed)

    }
    // console.log(ct);
    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: ct
    }

};

const Modellist_rate = async function (obj) {
    sql = "select * from scoreran where is_delete = 1 AND status_publish = 1 AND ref_member_scoreran_id = " + obj.id + " " +
        "ORDER BY scoreran.win_rate DESC";
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'Success',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    var ct = []
    var sortorder = 0
    for (var i = 0; i < insert_rows['result'].length; i++) {
        var sortorder = sortorder + 1
        if (insert_rows['result'][i].playrate == 0) {
            var win_calculate = "0.00"
        } else {
            var dd = (insert_rows['result'][i].win_rate / insert_rows['result'][i].playrate) * 100
            var win_calculate = parseFloat(dd).toFixed(2)
        }

        var packed = {
            sortorder: sortorder,
            member_id: insert_rows['result'][i].id,
            name: insert_rows['result'][i].name,
            // status_publish: insert_rows['result'][i].status_publish,
            // is_delete: insert_rows['result'][i].is_delete,
            playrate: insert_rows['result'][i].playrate,
            win_calculate: win_calculate
            // win_calculate: parseFloat(dd).toFixed(2)

        }
        ct.push(packed)

    }
    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: ct
    }

};

const Modelrandom = async function (obj) {
    // limit 4
    sql = "select * from scoreran where is_delete = 1 AND status_publish = 1 AND ref_member_scoreran_id = " + obj.id + " ORDER BY playday_rate ASC ,RAND() limit 4";
    // sql = "select * from scoreran where is_delete = 1 AND status_publish = 1 AND ref_member_scoreran_id = " + obj.id + " ORDER BY RAND() limit 4";
    // console.log(sql);
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'Success',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    // console.log(ct);
    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }

};

const Modelcount = async function (obj) {
    sql = "SELECT COUNT( id ) AS countmember " +
        "FROM scoreran WHERE is_delete = 1 AND status_publish = 1 AND ref_member_scoreran_id = "+obj.id+" ;"
    const rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            if (ex) {
                data = {
                    status: false,
                    message: ex.toString(),
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: '',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        console.log(err)
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });
    var sta
    var re
    if (rows['result'].length != 0) {
        sta = rows['status']
        // re = rows['result'][0]
        re = Object.values(JSON.parse(JSON.stringify(rows['result'])))
        re = re[0]
    } else {
        sta = false
        re = []
    }
    return data = {
        status: sta,
        message: rows['message'],
        result: re
    }

};

const Modelupdate_playdayrate = async function (obj) {
    sql = "UPDATE scoreran SET playday_rate = " + obj.playday_rate + " WHERE id = " + obj.scoreran_id + "";
    // console.log(sql);
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'Success',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        // console.log(err)
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modelcrateroom = async function (obj) {
    ddnow = moment(Date.now()).format('YYYY-MM-DD');
    sql = "INSERT INTO groups (groups_guid,createdate,ref_member_scoreran_id) " +
        "values ('" + uuid4() + "','" + ddnow + "'," + obj.id + ");"
    // console.log(sql);
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            if (ex) {
                // console.log(ex)
                if (ex.errno == 1062) {
                    message = 'Duplicate username.'
                } else {
                    message = ex.toString()
                }
                data = {
                    status: false,
                    message: message,
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: '',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        return err
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modelsaveroom = async function (obj) {
    // ddnow = moment(Date.now()).format('YYYY-MM-DD');
    sql = "INSERT INTO listgroup (scoreran_id,groups_id,sort) " +
        "values (" + obj.scoreran_id + "," + obj.groups_id + "," + obj.sort + ");"
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            if (ex) {
                // console.log(ex)
                if (ex.errno == 1062) {
                    message = 'Duplicate username.'
                } else {
                    message = ex.toString()
                }
                data = {
                    status: false,
                    message: message,
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: '',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        return err
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }

};

const Modeljoin_saveroom__update_playdayrate = async function (obj) {
    sql = "UPDATE scoreran SET playday_rate = " + obj.playday_rate + " WHERE id = " + obj.scoreran_id + "";
    // console.log(sql);
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'Success',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        // console.log(err)
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    sql2 = "INSERT INTO listgroup (scoreran_id,groups_id,sort) " +
        "values (" + obj.scoreran_id + "," + obj.groups_id + "," + obj.sort + ");"
    const insert_rows2 = await new Promise((resolve, reject) => {
        DB.query(sql2, (ex, rows) => {
            if (ex) {
                // console.log(ex)
                if (ex.errno == 1062) {
                    message = 'Duplicate username.'
                } else {
                    message = ex.toString()
                }
                data = {
                    status: false,
                    message: message,
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: '',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        return err
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const ModelSelectAdmin = async function (obj) {
    sql = "select * from member_scoreran " +
        "where id = " + obj.id + "";
    const rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            if (ex) {
                data = {
                    status: false,
                    message: ex.toString(),
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: '',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        console.log(err)
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });
    var sta
    var re
    if (rows['result'].length != 0) {
        sta = rows['status']
        // re = rows['result'][0]
        re = Object.values(JSON.parse(JSON.stringify(rows['result'])))
        re = re[0]
    } else {
        sta = false
        re = []
    }
    return data = {
        status: sta,
        message: rows['message'],
        result: re
    }

};

const Modelclean = async function (obj) {

    sql = "UPDATE scoreran SET playday_rate = 0 WHERE ref_member_scoreran_id = " + obj.id + "";
    // console.log(sql);
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'Success',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        // console.log(err)
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modelupday = async function (obj) {
    dnow = moment(Date.now()).format('YYYY-MM-DD');
    sql = "UPDATE member_scoreran SET playday = '" + dnow + "' WHERE id = " + obj.id + "";
    // console.log(sql);
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'Success',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        // console.log(err)
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modelcleanroom = async function (obj) {

    sql = "delete FROM groups WHERE ref_member_scoreran_id = " + obj.id + "";
    // console.log(sql);
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'Success',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        // console.log(err)
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modellist_random = async function (obj) {
    // sql = "select groups.id,scoreran.name,listgroup.sort "+
    // "FROM groups "+
    // "INNER JOIN listgroup ON groups.id = listgroup.groups_id "+
    // "INNER JOIN scoreran ON listgroup.scoreran_id = scoreran.id "+
    // "where groups.ref_member_scoreran_id = "+obj.id+" "+
    // "ORDER BY listgroup.groups_id ASC,listgroup.sort ASC";
    sql = "select groups.id,groups.status_play FROM groups " +
        "where groups.ref_member_scoreran_id = " + obj.id + " "
        // "ORDER BY groups.id ASC";
    // console.log(sql);
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'Success',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });
    var ct = []
    var sortorder = 0
    for (var i = 0; i < insert_rows['result'].length; i++) {
        var sortorder = sortorder + 1
        var packed = {
            sortorder: sortorder,
            scoreran_id: insert_rows['result'][i].id

        }
        ct.push(packed)

    }
    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }

};

const Modelgetlistgroup = async function (obj) {
    // sql = "select scoreran.id,scoreran.name,listgroup.sort " +
    //     "FROM groups " +
    //     "INNER JOIN listgroup ON groups.id = listgroup.groups_id " +
    //     "INNER JOIN scoreran ON listgroup.scoreran_id = scoreran.id " +
    //     "where listgroup.groups_id = " + obj.groups_id + " " +
    //     "ORDER BY listgroup.groups_id ASC,listgroup.sort ASC";
    sql = "select scoreran.id,scoreran.name,listgroup.sort " +
        "FROM listgroup " +
        "INNER JOIN scoreran ON listgroup.scoreran_id = scoreran.id " +
        "where listgroup.groups_id = " + obj.groups_id + " " +
        "ORDER BY listgroup.groups_id ASC,listgroup.sort ASC";
    // console.log(sql);
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'Success',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });
    var ct = []
    var sortorder = 0
    for (var i = 0; i < insert_rows['result'].length; i++) {
        var sortorder = sortorder + 1
        var packed = {
            sortorder: sortorder,
            scoreran_id: insert_rows['result'][i].id

        }
        ct.push(packed)

    }
    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }

};


const Modeleditpublish = async function (obj) {
    sql = "UPDATE scoreran SET status_publish = " + obj.status_publish + " " +
        "WHERE id = " + obj.id + " ";
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'เปลี่ยนสถานะสำเร็จ',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modelgetscoreran_ck = async function (obj) {
    sql = "select * from scoreran " +
        "where id = " + obj.id + "";
    const rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            if (ex) {
                data = {
                    status: false,
                    message: ex.toString(),
                    result: []
                }
                reject(data);
            } else {
                data = {
                    status: true,
                    message: '',
                    result: rows
                }
                resolve(data);
            }
        });
    }).catch(function (err) {
        console.log(err)
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });
    var sta
    var re
    if (rows['result'].length != 0) {
        sta = rows['status']
        // re = rows['result'][0]
        re = Object.values(JSON.parse(JSON.stringify(rows['result'])))
        re = re[0]
    } else {
        sta = false
        re = []
    }
    return data = {
        status: sta,
        message: rows['message'],
        result: re
    }

};

const Modeldeletescoreran = async function (obj) {
    sql = "DELETE FROM scoreran WHERE id = " + obj.id + " "
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'ลบสำเร็จ',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modelresetgroups = async function (obj) {
    sql = "DELETE FROM groups WHERE ref_member_scoreran_id = " + obj.member_id + " "
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'ลบสำเร็จ',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modelchangestatusgroups = async function (obj) {
    sql = "UPDATE groups SET status_play = 2 " +
        "WHERE id = " + obj.id + " ";
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'เลือกสนามเปลี่ยนสถานะ กำลังแข่งขัน สำเร็จ',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modelchangestatusgroups2 = async function (obj) {
    sql = "UPDATE groups SET status_play = 3 " +
        "WHERE id = " + obj.id + " ";
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'เลือกสนามเปลี่ยนสถานะ แข่งขันจบแล้ว สำเร็จ',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

const Modeladdscoreresult = async function (obj) {
    sql = "UPDATE scoreran SET playrate = " + obj.playrate + ",win_rate = " + obj.win_rate + " " +
        "WHERE id = " + obj.id + " ";
    const insert_rows = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
            data = {
                status: true,
                message: 'สรุปผลแข่ง สำเร็จ',
                result: rows
            }
            resolve(data);

        });
    }).catch(function (err) {
        data = {
            status: false,
            message: "error " + err.toString(),
            result: []
        }
        return data
    });

    return data = {
        status: insert_rows['status'],
        message: insert_rows['message'],
        result: insert_rows['result']
    }
};

module.exports = {
    Modelcreate_member,
    Modellist_member,
    Modellist_rate,
    Modelrandom,
    Modelcount,
    Modelupdate_playdayrate,
    Modelcrateroom,
    Modelsaveroom,
    ModelSelectAdmin,
    Modelupday,
    Modelclean,
    Modelcleanroom,
    Modellist_random,
    Modelgetlistgroup,
    Modeleditpublish,
    Modelgetscoreran_ck,
    Modeldeletescoreran,
    Modelresetgroups,
    Modelchangestatusgroups,
    Modeladdscoreresult,
    Modelchangestatusgroups2,
    Modeljoin_saveroom__update_playdayrate


}