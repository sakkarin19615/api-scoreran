const express = require('express')
const routerAuth = express.Router()
const MemberController = require('../Controller/Member.js')
const SuperadminController = require('../Controller/Superadmin.js')

const auth = require('../auth/auth');

routerAuth.post('/createadmin', SuperadminController.createAdmin)
routerAuth.post('/login', SuperadminController.login)

routerAuth.post('/createmember',auth.verifyTokenPG,MemberController.create_member)
routerAuth.get('/get_listmember',auth.verifyTokenPG,MemberController.list_member)
routerAuth.get('/get_listrate',auth.verifyTokenPG,MemberController.list_rate)
routerAuth.get('/get_random',auth.verifyTokenPG,MemberController.random)
routerAuth.get('/get_listrandom',auth.verifyTokenPG,MemberController.list_random)
routerAuth.put('/edit_statuspublish/:scoreran_id',auth.verifyTokenPG,MemberController.editstatus_publish)
routerAuth.delete('/delete_scoreran/:scoreran_id', auth.verifyTokenPG, MemberController.deletescoreran)
routerAuth.get('/change_room/:groups_id',auth.verifyTokenPG,MemberController.changeroom)
routerAuth.post('/addscore_result', auth.verifyTokenPG, MemberController.addscoreresult)
routerAuth.get('/member_scorerandetail', auth.verifyTokenPG, MemberController.scorerandetail)

module.exports = { routerAuth } 