const jwt = require('jsonwebtoken');
const { DB } = require('../configs/db.connect');

async function verifyTokenPG(req, res, next) {
  const bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    if (bearer[0] != 'Bearer') {
      res.status(403).json({
        status: false,
        message: "Invalid access_token",
      });
    } else {
      const bearerToken = bearer[1];
      const obj = {
        access_token: bearerToken
      }
      sql = "select * from member_scoreran_token RIGHT JOIN member_scoreran ON member_scoreran_token.ref_member_scoreran_id = member_scoreran.id where access_token = '" + obj.access_token + "'"
      const results = await new Promise((resolve, reject) => {
        DB.query(sql, (ex, rows) => {
          if (ex) {
            data = {
              status: false,
              message: ex.toString(),
              result: []
            }
            reject(data);
          } else {
            // console.log(rows.length)
            if (rows.length == 0) {
              status = false
              message = 'Invalid Access Token.'
            } else {
              status = true
              message = ''
            }
            data = {
              status: status,
              message: message,
              result: Object.values(JSON.parse(JSON.stringify(rows)))
            }
            // console.log(data);
            resolve(data);
          }
        });
      }).catch(function (err) {
        // console.log(err)
        return err
      });
      // console.log(results)
      if (results.status == false) {
        res.status(403).json({
          status: false,
          message: "Invalid access_token",
        });
      } else {
        // console.log(results)
        if (results.result[0].is_deleted == 99) {
          res.status(403).json({
            status: false,
            message: "Invalid access_token",
          });
        } else {
          jwt.verify(bearerToken, 'member_scoreran', (err, authData) => {
            // console.log(authData.payload.member_id)
            if (err) {
              // console.log('hi')
              res.status(403).json({
                status: false,
                message: "Invalid access_token",
              });
            } else {
              try {
                req.user_id = authData.payload.member_id
                next();
              } catch (error) {
                res.status(403).json({
                  status: false,
                  message: "Invalid access_token",
                });
              }
            }
          });
        }
      }
    }

  } else {
    res.status(403).json({
      status: false,
      message: "Invalid access_token",
    });
  }
}


module.exports.verifyTokenPG = verifyTokenPG;