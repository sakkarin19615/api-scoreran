const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const jwt = require('jsonwebtoken');
const { MongoClient } = require('mongodb');
const moduleDB = require('./configs/db.config');
const auth = require('./auth/auth');
const checkdata = require("./verifydata/verifydata");
const store = require('./service/store');
require('dotenv').config()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


app.get('/GetExternalApi', (req, res) => {
    store.ApiGetdata().then(function (response) {
        if (response.status == 200) {
            res.status(response.status).json({
                status: true,
                message: 'Success GetExternalApi',
                result: response.data
            });
        } else {
            res.status(response.status).json({
                status: false,
                message: response.statusText,
                result: null
            });
        }
    })
})

app.get('/PostExternalApi', (req, res) => {
    store.ApiPostdata(req.body).then(function (response) {
        if (response.status == 200) {
            console.log(response)
            res.status(response.status).json({
                status: true,
                message: 'Success PostExternalApi',
                result: response.data
            });
        } else {
            console.log(response)
            res.status(response.status).json({
                status: false,
                message: response.statusText,
                result: null
            });
        }
    })
})



app.listen(3001, () => {
    console.log('Start server at port 3001.')
})