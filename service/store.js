const axios = require('axios')
require('dotenv').config()
var moment = require('moment');

const ApiGetdata = async (data) => {
    return new Promise(resolve => {
        setTimeout(async () => {
            var CurrentDate = moment().unix();
            try {
                var response = await axios.get(
                    process.env.HOSTAPI + "/posts"
                )
                response.status = 200
                resolve(response)
            } catch (error) {
                resolve(error.response)
            }
        }, 0);
    });
}


const ApiPostdata = async (data) => {
    return new Promise(resolve => {
        setTimeout(async () => {
            var CurrentDate = moment().unix();
            let reqData = {
                title: 'foo',
                body: 'bar',
                userId: 1,
            }
            try {
                var response = await axios.post(
                    process.env.HOSTAPI + "/posts", reqData
                )
                response.status = 200
                resolve(response)
            } catch (error) {
                console.log(error. response. data)
                console.log(error. response. status);
                resolve(error.response)
            }
        }, 0);
    });
}


module.exports = { ApiGetdata,ApiPostdata }