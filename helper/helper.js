bcrypt = require('bcrypt');
SALT_WORK_FACTOR = 10;

const sum = function name(a, b) {
    return a + b;
};

const encryptionData = async function encryption(password) {
    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR)
    const hash = await bcrypt.hash(password, salt)
    return hash
};

const decryptionData = async function decryption(candidatePassword, dbPsw) {
    return bcrypt.compare(candidatePassword, dbPsw).then((result) => {
        if (result) {
            // console.log("authentication successful")
            return true
        } else {
            // console.log("authentication failed. Password doesn't match")
            return false
        }
    })
        .catch(function (err) {
            // console.log("err")
            return false
        })
};


module.exports = {
    sum, encryptionData, decryptionData
}