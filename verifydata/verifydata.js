const Joi = require("@hapi/joi");

const loginSchema = Joi.object({
    username: Joi.string().max(6).required().messages({ "string.max": "55555" }),
});

const vs_CreateBook = Joi.object({
    name: Joi.string().min(2).required(),
    address: Joi.string().min(2).required()
});


const depositeSchema = Joi.object({
    username: Joi.string().max(6).required().messages({ "string.max": "55555" }),
    amount: Joi.number().integer().min(1).required().messages({ "number.min": "จำนวนขั้นต่ำ 1 บาท" }),
});



module.exports = { loginSchema,vs_CreateBook,depositeSchema }