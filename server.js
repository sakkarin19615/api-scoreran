const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const PORT = process.env.PORT || 4001;
const Route = require('./routers/router')
require('./configs/db.connect');

var cors = require('cors')
app.use(cors({origin: '*',credentials: true}))
global.__basedir = __dirname;
// global.__baseMedia = "/api/scoreran/media/";
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use('/api/scoreran', Route.routerAuth)
// app.use('/api/scoreran/media', express.static('media'))
app.listen(PORT, () => console.log(`Running server on port: ${PORT}`));